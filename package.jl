using Pkg
Pkg.add("Libdl")
Pkg.add("Printf")
Pkg.add("Unicode")
Pkg.add("Dates")
Pkg.add("Gadfly")
Pkg.add("CSV")
Pkg.add("Crayons")
Pkg.add("FIGlet")
