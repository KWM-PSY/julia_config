This repository is part of PlatformCommander, follow this link for more information: https://gitlab.com/KWM-PSY/platform-commander

# Julia configuration

## Step 1:
* Download the two .jl files above and the packages below and save/unpack them into a directory of your liking.

- [MOO-C-EMU](https://gitlab.com/KWM-PSY/moo-c-emu) (files needed to create the libmoogskt.so file in c)
- [MOOG](https://gitlab.com/KWM-PSY/moog) (this package defines the messages ID that are implemented on the server and a call to a     network socket implemented in C)
- [MoogCom](https://gitlab.com/KWM-PSY/moogcom) (this is the module that implements the communication with the server)
- [Exp_helper](https://gitlab.com/KWM-PSY/exp_helper) (this module defines functions and structures that support an efficient implementation of experiments)
- [Questionnaires](https://gitlab.com/KWM-PSY/questionnaires) (this module is thought to implement Questionnaires frequently used)

## Step 2:
* Install julia either by following the instructions on their website [julia](https://julialang.org/downloads/platform/) or executing the command bellow (the julia version installed through the terminal is usually an older version)
    ```
    sudo apt-get install julia
    ```

* Open a new terminal and execute julia. You should see a similar view as the one below.
    ```
        _       _ _(_)_    | Documentation: https://docs.julialang.org
       (_)     | (_) (_)   |
        _ _ _ _| |_ __ _   | Type "?" for help, "]?" for Pkg help.
       | | | | | | |/ _` | |
       | | |_| | | | (_| | | Version 1.6.3
      _/ | __'_|_|_| __'_| | Commit 4bf946a9ca (7 days old master)
     |__/                  |
    julia>
    ```

* Exit julia by typing exit() or pressing crtl+d

* Now install the needed packages by executing the package.jl file in a terminal (make sure you're in the right directory)
    ```
    julia package.jl
    ```

## Step 3:
* Create the config directory in .julia and move the startup.jl file in it.
    ```
    sudo mkdir /home/<usr>/.julia/config
    ```
  and
    ```
    sudo cp /home/<usr>/<PathTo>/startup.jl /home/<usr>/.julia/config/
    ```

* Once you have copied the startup.jl file into .julia/config you will need to change its contents. Add the corresping path to the previously downloaded packages
    ```
    push!(LOAD_PATH, "/home/<usr>/<PathTo>/moo-c-emu-main/c")
    push!(LOAD_PATH, "/home/<usr>/<PathTo>/moogcom-master/src")
    push!(LOAD_PATH, "/home/<usr>/<PathTo>/moog-master/src")
    push!(LOAD_PATH, "/home/<usr>/<PathTo>/exp_helper-master/src")
    push!(LOAD_PATH, "/home/<usr>/<PathTo>/questionnaires-master/src")
    ```

* If everything is correct in the startup.jl file open a new Terminal and execute
    ```
    julia
    ```
  followed by
    ```
    using MOOG
    ```
  you should get this reply
    ```
    [ info: Precompiling MOOG    [top-level]
    ```
  leave julia by executing exit() or pressing crtl+D

## Step 4
* You will now have to create the file libmoogskt.so. Go to the directory /moo-c-emu/c/ in your terminal and type the command
    ```
    make
    ```
  please make sure the file was created. You should find it in /moo-c-emu/c/

* Now open your startup.jl file again uncomment the following and adjust the path (Make sure the path is correct!)
    ```
    # set the user specific constants
    const MOOGSKT_C_MODULE="/home/<usr>/<PathTo>/moo-c-emu-main/c/libmoogskt.so"
    ```
  Save and exit.

## Step 5 (This step is only necessairy if you wish to run the platform commander from one device)
* Open the MOOG.jl file in of the directory moog-master/src and change the numbers for the SERVER_INPORT and SERVER_OUTPORT
  ```
  const SERVER_INPORT= 11332
  const SERVER_OUTPORT= 11331
  ```
  Save and exit

## The end
* This concludes the julia configuration for the MotionPlatform emulator. We wish you joyful simulations!



