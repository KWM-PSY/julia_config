# ===========================================================
#  Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
#  daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
# 
#  This file is part of Platform Commander.
# 
#  Platform Commander is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  Platform Commander is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
#  ===========================================================

#  -----------------------------------------------------------
# place this file in: .julia/config/startup.jl
#  -----------------------------------------------------------


#  -----------------------------------------------------------
# make client modules visible to julia
push!(LOAD_PATH, "/home/<usr>/<PathTo>//moo-c-emu/c/")
push!(LOAD_PATH, "/home/<usr>/<PathTo>/moogcom-master/src/")
push!(LOAD_PATH, "/home/<usr>/<PathTo>/moog-master/src/")
push!(LOAD_PATH, "/home/<usr>/<PathTo>/exp_helper-master/src/")
push!(LOAD_PATH, "/home/<usr>/<PathTo>/questionnaires-master/src/")

# set the user specific constants
#const MOOGSKT_C_MODULE="/home/<usr>/<PathTo>/moo-c-emu/c/libmoogskt"

#  -----------------------------------------------------------
# client ip-address
const LOCAL_ADDRESS="127.0.0.1"

#  -----------------------------------------------------------
# server ip-address
# up to four addresses can be defined. platform commander automatically checks
# which of these addresses is connected. this allows for an easy worflow when 
# multiple instances of the platform commander are used (e.g. server and raspberry
# pie). 
const SERVER_MOOG_ADDRESS1="127.0.0.1"
const SERVER_MOOG_ADDRESS2=""
const SERVER_MOOG_ADDRESS3=""
const SERVER_MOOG_ADDRESS4=""

#  -----------------------------------------------------------
#  print this to verify that startup file has been loaded
println("====> Congratulation! You succesfully loaded the startup file. You may change this text")

